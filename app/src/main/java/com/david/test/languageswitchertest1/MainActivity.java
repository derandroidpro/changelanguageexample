package com.david.test.languageswitchertest1;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TextView;

import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    // Schlüssel, der verwendet wird, um das Länderkürzel in den SharedPreferences zu speichern:
    private final String COUNTRY_CODE_PREFERENCE_KEY = "COUNTRY_CODE_PREFERENCE_KEY";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Sprache der App einstellen:
        setupLanguage();

        // der Titel der ActionBar muss erneut gesetzt werden, da dieser bereits aus den Strings geladen wird, BEVOR die Methode "onCreate" dieser Activity aufgerufen wird:
        getSupportActionBar().setTitle(getString(R.string.app_name));

        // Jetzt noch den Text des TextViews setzen:
        TextView tv = findViewById(R.id.textView);
        tv.setText(getString(R.string.lang_info_string));
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.optionsmenu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.item_german:
                saveLanguageSettingAndRestart("de");
                break;
            case R.id.item_english:
                saveLanguageSettingAndRestart("en");
                break;
            case R.id.item_spanish:
                saveLanguageSettingAndRestart("es");
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    private void saveLanguageSettingAndRestart(String countryCode){
        // zuerst muss das gewählte Länderkürzel dauerhaft in den SharedPreferences gespeichert werden, damit es nach einem Neustart der App erneut gelesen werden kann:
        PreferenceManager.getDefaultSharedPreferences(this).edit().putString(COUNTRY_CODE_PREFERENCE_KEY, countryCode).commit();


        // Anschließend muss die Activity neugestartet werden, damit die Änderung wirksam wird:
        startActivity(new Intent(this, MainActivity.class));
        finish();
    }

    private void setupLanguage(){
        SharedPreferences sharedPrefs = PreferenceManager.getDefaultSharedPreferences(this);

        // überprüfen, ob ein Länderkürzel gespeichert ist, ansonsten einfach nichts tun (System stellt die Systemsprache automatisch ein):
        if(sharedPrefs.contains(COUNTRY_CODE_PREFERENCE_KEY)) {
            // Bei JEDEM Start der App muss das Länderkürzel aus den SharedPreferences gelesen und mithilfe dessen die Sprache der App eingestellt werden:
            String countryCode = sharedPrefs.getString(COUNTRY_CODE_PREFERENCE_KEY, "en");
            Log.d("Language Code", countryCode);

            Locale userLocale = new Locale(countryCode);
            Resources res = getResources();
            Configuration config = res.getConfiguration();
            config.setLocale(userLocale);
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
    }
}
